import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { baseURL, LOGIN } from "../../../Api/Api";
import axios from "axios";
import "./Auth.css";
import LoadingSubmit from "../../Loading/Loading";
import Cookie from "cookie-universal";
import { Form } from "react-bootstrap";
import Employee from "../Dashboard/Employee";

export default function Login() {
  // states
  const [form, setForm] = useState({
    email: "",
    password: "",
  });

  const navigate = useNavigate();

  // loading
  const [loading, setLoading] = useState(false);

  // Cookie
  const cookie = Cookie();

  //  Err
  const [err, setErr] = useState("");

  // handle Form Change
  function handleChange(e) {
    setForm({ ...form, [e.target.name]: e.target.value });
  }

  // handle Submit
  async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    try {
      const res = await axios.post(`${baseURL}/${LOGIN}`, form); // DATA IS FORM http://127.0.0.1:4000/api/login
      console.log(res);
      setLoading(false);
      const token = res.data.token;
      const id =  res.data.user._id;
      const name = res.data.user.name;
      const role = res.data.user.role;
      cookie.set("id" , id );
      cookie.set("name", name);
      cookie.set("e-commerce", token);
      cookie.set("role", role);
      const go = role === "2002" ? "users" : "employee";
      navigate(`/dashboard/${go}`);
      
    } catch (err) {
      setLoading(false);
      // i will remove comment when get real backend
      if (err.response.status === 401) {
        setErr(err.response.data.message);
      } else {
        setErr(err.response.data.message);
      }
    }
  }
  return (
    <>
      {loading && <LoadingSubmit />}
      <div className="container">
        <div className="row" style={{ height: "100vh" }}>
          <Form className="form" onSubmit={handleSubmit}>
            <div className="custome-form">
              <h1>Login Now</h1>
              <Form.Group
                className="form-custom"
                controlId="exampleForm.ControlInput2"
              >
                <Form.Control
                  value={form.email}
                  onChange={handleChange}
                  type="email"
                  name="email"
                  placeholder="Enter Your Email.."
                  required
                />
                <Form.Label>Email:</Form.Label>
              </Form.Group>
              <Form.Group
                className="form-custom"
                controlId="exampleForm.ControlInput3"
              >
                <Form.Control
                  value={form.password}
                  onChange={handleChange}
                  type="password"
                  name="password"
                  placeholder="Enter Your Password.."
                  minLength="8"
                  required
                />
                <Form.Label>Password:</Form.Label>
              </Form.Group>
              <button className="button btnPrimary" type="submit">
                Login
              </button>
              {err && <span className="error"> {err} </span>}
            </div>
          </Form>
        </div>
      </div>
    </>
  );
}
