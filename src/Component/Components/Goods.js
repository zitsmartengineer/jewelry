import { IoIosBasket } from "react-icons/io";
import { FaEye } from "react-icons/fa";

export default function Goods(props) {
  return (
    <div style={{minWidth: "250px" , width: "20%" , maxWidth: "400px" , margin: "0 1rem"}}>
      <div className=" one-product">
        <img className="img1" src={props.image1} alt="" />
        <div className="img2 position-relative">
          <img src={props.image2} alt="" />
          <IoIosBasket size={24} className="position-absolute pos-icon-1" />
          <FaEye size={24} className="position-absolute pos-icon-2" />
        </div>
      </div>
      <div className="dark fs-5"> {props.karat} karat </div>
      <div className="dark fs-5"> ${props.price} </div>
    </div>
  );
}
