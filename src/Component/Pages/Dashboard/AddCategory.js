import { useState } from "react";
import { Form } from "react-bootstrap";
import {  rootURL } from "../../../Api/Api";
import Loading from "../../Loading/Loading";
import axios from "axios";

export default function AddCategory() {
  const [title, setTitle] = useState('');
  const [image, setImage] = useState('');

  // // ERR
  const [err, setErr] = useState("");

  // Loading
  const [loading, setLoading] = useState(false);

  // SUBMIT FOR UPDATE INFO
  async function handleSubmit(e) {
    e.preventDefault(); 
    const form = new FormData();
    form.append("title", title);
    form.append("image", image);
    setLoading(true);
    try {
      // const res = await axios.post(`${Cat}/add`, form);
      const res = await axios.post(`${rootURL}/products/add-product`, form);
      console.log("res: " , res);
      setLoading(false);
      // window.location.pathname = "/dashboard/categories";
    } catch (err) {
      setLoading(false);
      setErr(err.response.data);
      console.log("err ", err);

    }
  }

  return (
    <>
      {loading && <Loading />}
      <Form className="bg-control w-100 p-3" onSubmit={handleSubmit}>
        <Form.Group
          className="mb-3 text-start"
          controlId="exampleForm.ControlInput1"
        >
          <Form.Label>Title:</Form.Label>
          <Form.Control
            className="bg-input-control"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            type="text"
            name="title"
            placeholder="Enter Title.."
            minLength="3"
            required
          />
        </Form.Group>
        <Form.Group className="mb-3 text-start" controlId="image">
          <Form.Label>Image:</Form.Label>
          <Form.Control
            className="bg-input-control"
            value={image}
            onChange={(e) => setImage(e.target.value)}
            type="file"
            name="image"
            
          />
        </Form.Group>
        <button className="button btn-blue" type="submit">
          Save
        </button>
        {err && <p className="error"> {err} </p>}
      </Form>
    </>
  );
}
