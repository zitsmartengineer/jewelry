export const baseURL = `http://127.0.0.1:4000/api`;
export const rootURL = `http://127.0.0.1:4000`;

export const REGISTER = "register";
export const LOGIN = "login";
export const LOGOUT = "logout";

export const USERS = "users";
export const USER = "user";

export const CAT = "categories";
export const Cat = "category";

// export const GOOGLE_CALL_BACK = "auth/google/callback";
