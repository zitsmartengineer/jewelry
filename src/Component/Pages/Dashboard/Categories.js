import { useEffect, useState } from "react";
import { CAT, rootURL } from "../../../Api/Api";
import { Link } from "react-router-dom";
import { Axios } from "../../../Api/Axios";
import Cookie from "cookie-universal";
import TableShow from "./Table";
import axios from "axios";

export default function Users() {
  const [categories, setCategories] = useState([]);
console.log("cate:" , categories);
  // FOR FETCH DATA EACH ACTION DELETE
  const [deleteCategory, setDeleteCategory] = useState(false);

  // FETCH DATA OF USERS
  useEffect(() => {
    // Axios.get(`/${CAT}`)
    axios.get(`${rootURL}/products/`)
      .then((data) => setCategories(data.data))
      .then((data) => console.log("product" , data.data ) )
      .catch((err) => console.log(err));
  }, [deleteCategory]);

  // HANDLE handleDelete
  async function handleDelete(id) {
    try {
      const res = await Axios.delete(`${CAT}/${id}`);
      setCategories((prev) => prev.filter((item) => item._id !== id));
    } catch (err) {
      console.log(err);
    }
  }
  const header = [
    {
      key: "title",
      name: "Title",
    },
    {
      key: "image",
      name: "Image",
    },
  ];
  return (
    <div style={{ background: "#dedded" }} className="p-3 w-100">
      <div className="d-flex justify-content-between mb-2">
        <h1 className="color-555"> Users Page </h1>
        <Link className="btn-blue" to={"/dashboard/category/add"}>
          Add Category
        </Link>
      </div>
      <TableShow header={header} data={categories} delete={handleDelete} />
    </div>
  );
}
