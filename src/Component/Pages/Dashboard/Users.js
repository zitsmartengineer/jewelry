import { useEffect, useState } from "react";
import { USER, USERS } from "../../../Api/Api";
import { Link } from "react-router-dom";
import { Axios } from "../../../Api/Axios";
import Cookie from "cookie-universal";
import TableShow from "./Table";

export default function Users() {
  const [users, setUsers] = useState([]);

  // FOR CHECK IF NOUSER FOUND OR NOT
  const [noUsers, setNoUsers] = useState(false);

  // FOR FETCH DATA EACH ACTION DELETE
  const [deleteUser, setDeleteUser] = useState(false);

  // FETCH DATA OF USERS
  useEffect(() => {
    Axios.get(`/${USERS}`)
      .then((data) => setUsers(data.data))
      .then(() => setNoUsers(true))
      .catch((err) => console.log(err));
  }, [deleteUser]);

  const cookie = Cookie();
  const currentUser = cookie.get("name");

  // HANDLE handleDelete
  async function handleDelete(id) {
    try {
      const res = await Axios.delete(`${USER}/${id}`);
      setUsers((prev) => prev.filter((item) => item._id !== id));
    } catch (err) {
      console.log(err);
    }
  }
  const header = [
    {
      key: "name",
      name: "Username",
    },
    {
      key: "email",
      name: "Email",
    },
    {
      key: "role",
      name: "Role",
    },
  ];
  return (
    <div style={{background: "#dedded"}} className="p-3 w-100">
      <div className="d-flex justify-content-between mb-2">
        <h1 className="color-555"> Users Page </h1>
        <Link className="btn-blue" to={"/dashboard/user/add"}>
          Add User
        </Link>
      </div>
      <TableShow
        header={header}
        data={users}
        currentUser={currentUser}
        delete={handleDelete}
        noUsers={noUsers}
      />
    </div>
  );
}
