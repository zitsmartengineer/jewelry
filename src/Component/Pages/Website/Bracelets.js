import Navbar from "../NavbarAndFooter/Navbar";
import Footer from "../NavbarAndFooter/Footer";
import Goods from "../../Components/Goods";

// IMPORT IMAGES
import imageFar1 from "../../../Css/Bracelets/1far.jpg";
import imageClosed1 from "../../../Css/Bracelets/1closed.jpg";
import imageFar2 from "../../../Css/Bracelets/2far.jpg";
import imageClosed2 from "../../../Css/Bracelets/2closed.jpg";
import imageFar3 from "../../../Css/Bracelets/3far.jpg";
import imageClosed3 from "../../../Css/Bracelets/3closedd.jpg";
import imageFar4 from "../../../Css/Bracelets/4far.jpg";
import imageClosed4 from "../../../Css/Bracelets/4closed.jpg";
import imageFar5 from "../../../Css/Bracelets/5far.jpg";
import imageClosed5 from "../../../Css/Bracelets/5closed.jpg";
import imageFar6 from "../../../Css/Bracelets/6far.jpg";
import imageClosed6 from "../../../Css/Bracelets/6closed.jpg";

// ===============

export default function Bracelets() {
  // FAKE DATA
  const data = [
    {
      imageFar: imageFar1,
      imageClosed: imageClosed1,
      karat: "18",
      price: "1,200,000",
    },
    {
      imageFar: imageFar2,
      imageClosed: imageClosed2,
      karat: "24",
      price: "1,280,000",
    },
    {
      imageFar: imageFar3,
      imageClosed: imageClosed3,
      karat: "24",
      price: "2,200,000",
    },
    {
      imageFar: imageFar4,
      imageClosed: imageClosed4,
      karat: "20",
      price: "1,200,000",
    },
    {
      imageFar: imageFar5,
      imageClosed: imageClosed5,
      karat: "18",
      price: "4,200,000",
    },
    {
        imageFar: imageFar6,
        imageClosed: imageClosed6,
        karat: "18",
        price: "4,200,000",
      },
  ];

  return (
    <>
      <Navbar />
      <h1
        className=" font-6xl height-16 pt-5 text-start m-5 d-flex align-items-end  "
      >
        BRACELETS
      </h1>
      <section style={{margin: "5rem 0"}}>
        <div
          style={{ gap: "1rem" }}
          className="d-flex flex-wrap justify-content-center "
        >
          {data.map((item, key) => (
            <Goods
              key={key}
              image1={item.imageFar}
              image2={item.imageClosed}
              karat={item.karat}
              price={item.price}
            />
          ))}
        </div>
      </section>
      <Footer />
    </>
  );
}
