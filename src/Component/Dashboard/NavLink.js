import { FaUsers } from "react-icons/fa";
import {BsPersonPlusFill} from "react-icons/bs";
import {MdOutlineAddShoppingCart, MdProductionQuantityLimits} from "react-icons/md";

export const links = [
  {
    name: " All Users",
    path: "users",
    icon: <FaUsers size={30} />,
    role: "2002",
  },
  {
    name: "Add User",
    path: "/dashboard/user/add",
    icon: <BsPersonPlusFill size={30} />,
    role: "2002",
  },
  {
    name: "Categories",
    path: "/dashboard/categories",
    icon: <MdProductionQuantityLimits size={30} />,
    role: ["2010" , "2002"],
  },
  {
    name: "Add Category",
    path: "/dashboard/category/add",
    icon: <MdOutlineAddShoppingCart size={30} />,
    role:  "2002",
  },
  {
    name: "Employee",
    path: "/dashboard/employee",
    icon: <BsPersonPlusFill size={30} />,
    role: ["2002" , "2020"],
  },
];
