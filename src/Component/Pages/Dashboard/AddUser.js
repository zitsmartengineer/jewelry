import { useState } from "react";
import { Form } from "react-bootstrap";
import { Axios } from "../../../Api/Axios";
import {  USER } from "../../../Api/Api";
import Loading from "../../Loading/Loading";

export default function AddUser() {
  const [user, setUser] = useState({
    name: "",
    email: "",
    password: "",
    role: "",
  });

  // ERR
  const [err, setErr] = useState("");

  // Loading
  const [loading, setLoading] = useState(false);

  // SUBMIT FOR UPDATE INFO
  async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    try {
      const res = await Axios.post(`${USER}/add`, {
        name: user.name,
        email: user.email,
        password: user.password,
        role: user.role,
      });
      setLoading(false);
      window.location.pathname = "/dashboard/users";
    } catch (err) {
      setLoading(false);
      setErr(err.response.data);
      if (err.response.status === 300) {
        setErr("Email is already been taken");
      } else {
        setErr("Internal Server ERR");
      }
    }
  }

  //   HANDLE CHANGE IN FORM
  function handleChange(e) {
    setUser({ ...user, [e.target.name]: e.target.value });
  }
  return (
    <>
      {loading && <Loading />}
      <Form className="bg-control w-100 p-3" onSubmit={handleSubmit}>
        <Form.Group
          className="mb-3 text-start"
          controlId="exampleForm.ControlInput1"
        >
          <Form.Label>User Name:</Form.Label>
          <Form.Control
          className="bg-input-control"
            value={user.name}
            onChange={handleChange}
            type="text"
            name="name"
            placeholder="Enter Your Name.."
            minLength="3"
            required
          />
        </Form.Group>

        <Form.Group
          className="mb-3 text-start"
          controlId="exampleForm.ControlInput2"
        >
          <Form.Label>Email:</Form.Label>
          <Form.Control
          className="bg-input-control"
            value={user.email}
            onChange={handleChange}
            type="email"
            name="email"
            placeholder="Enter Your Email.."
            required
          />
        </Form.Group>
        <Form.Group
          className="mb-3 text-start"
          controlId="exampleForm.ControlInput3"
        >
          <Form.Label>Password:</Form.Label>
          <Form.Control
          className="bg-input-control"
            value={user.password}
            onChange={handleChange}
            type="password"
            name="password"
            placeholder="Enter Your Password.."
            minLength="8"
            required
          />
          <Form.Group
            className="mb-3 text-start"
            controlId="exampleForm.ControlInput4"
            
          >
            <Form.Label>Role:</Form.Label>
            <Form.Select name="role" className="bg-input-control" value={user.role} onChange={handleChange}>
              <option defaultValue={""}>
                Select Role
              </option>
              <option value="2002"> Admin </option>
              <option value="2010"> Products Manager </option>
              <option value="2020"> Employee </option>
              <option value="2023"> User </option>
            </Form.Select>
          </Form.Group>
        </Form.Group>
        <button className="button btn-blue" type="submit">
          Save
        </button>
        {err && <p className="error"> {err} </p>}
      </Form>
    </>
  );
}
