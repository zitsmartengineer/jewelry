import { NavLink, useNavigate } from "react-router-dom";
import "./bars.css";
import { Menu } from "../Context/MenuContext";
import { useContext, useEffect, useState } from "react";
import { WindowSize } from "../Context/WindowContext";
import { Axios } from "../../Api/Axios";
import { USER } from "../../Api/Api";
import Cookie from "cookie-universal";
import { links } from "./NavLink";


export default function Sidebar() {
  const menu = useContext(Menu);
  const isOpen = menu.isOpen;

  const windowContext = useContext(WindowSize);
  const windowSize = windowContext.windowSize;

  const [user, setUser] = useState({
    name: "",
    email: "",
    password: "",
    role: "",
  });
  const cookie = Cookie();
  const id = cookie.get("id");
  const Navigate = useNavigate();
  useEffect(() => {
    Axios.get(`${USER}/${id}`)
      .then((data) => setUser(data.data))
      .catch(() => Navigate("/login", { replace: true }));
  }, []);
  console.log("user: ", user.role);
  return (
    <>
      <div
        style={{
          background: "rgba(3, 142, 220, 0.2)",
          position: "fixed",
          top: "0",
          left: "0",
          bottom: "0",
          right: "0",
          display: windowSize < "768" && isOpen ? "block" : "none",
        }}
      ></div>
      <div
        className="side-bar pt-3"
        style={{
          left: windowSize < "768" ? (isOpen ? 0 : "-100%") : 0,
          width:
            windowSize < "768"
              ? "fit-content"
              : isOpen
              ? "220px"
              : "fit-content",
          position: windowSize < "768" ? "fixed" : "sticky",
        }}
      >
        {links.map(
          (link, key) =>
            link.role.includes(user.role) && (
              <NavLink
                key={key}
                to={link.path}
                className="d-flex align-items-center gap-2 side-bar-link"
                style={{
                  borderRadius: !isOpen || windowSize < 768 ? "50%" : "10px",
                  padding: !isOpen || windowSize < 768 ? "10px" : "10px 2px",
                }}
              >
                {link.icon}
                {isOpen && windowSize > 768 && (
                  <p className="m-0"> {link.name} </p>
                )}
              </NavLink>
            )
        )}
      </div>
    </>
  );
}
