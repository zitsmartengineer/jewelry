import "./App.css";
import "./index.css";

import HomePage from "./Component/Pages/Website/HomePage";
import Login from "./Component/Pages/Auth/Login";
import Register from "./Component/Pages/Auth/Register";
import Users from "./Component/Pages/Dashboard/Users";
import User from "./Component/Pages/Dashboard/User";
import { Route, Routes } from "react-router-dom";
import Dashboard from "./Component/Pages/Dashboard/Dashboard";
import RequireAuth from "./Component/Pages/Auth/RequireAuth";
import AddUser from "./Component/Pages/Dashboard/AddUser";
import Employee from "./Component/Pages/Dashboard/Employee";
import Err404 from "./Component/Pages/Auth/404";
import RequireBack from "./Component/Pages/Auth/RequireBack";
import AddCategory from "./Component/Pages/Dashboard/AddCategory";
import Categories from "./Component/Pages/Dashboard/Categories";
import Rings from "./Component/Pages/Website/Rings";
import Bracelets from "./Component/Pages/Website/Bracelets";
import Earrings from "./Component/Pages/Website/Earrings";
import Necklaces from "./Component/Pages/Website/Necklaces";
import About from "./Component/Pages/Website/About";
import Contact from "./Component/Pages/Website/Contact";

function App() {
  return (
    <div className="App">
      <Routes>
        {/* Public Routes */}
        <Route path="/" element={<HomePage />} />
        <Route path="/about" element={<About />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/rings" element={<Rings />} />
        <Route path="/bracelets" element={<Bracelets />} />
        <Route path="/earrings" element={<Earrings />} />
        <Route path="/necklaces" element={<Necklaces />} />
        <Route element={<RequireBack />}>
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
        </Route>
        <Route path="/*" element={<Err404 />} />
        {/* Protected Routes */}
        <Route element={<RequireAuth allowedRole={["2002", "2020" , "2010"]} />}>
          <Route path="/dashboard" element={<Dashboard />}>
            <Route element={<RequireAuth allowedRole={"2002"} />}>
              <Route path="users" element={<Users />} />
              <Route path="users/:id" element={<User />} />
              <Route path="user/add" element={<AddUser />} />
            </Route>
            <Route element={<RequireAuth allowedRole={["2002", "2010"]} />}>
              <Route path="categories" element={<Categories />} />
              <Route path="category/add" element={<AddCategory />} />
            </Route>
            <Route element={<RequireAuth allowedRole={["2020", "2002"]} />}>
              <Route path="employee" element={<Employee />} />
            </Route>
          </Route>
        </Route>
      </Routes>
    </div>
  );
}

export default App;
