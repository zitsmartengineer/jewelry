import { Table } from "react-bootstrap";
import { AiFillDelete } from "react-icons/ai";
import { FaPenToSquare } from "react-icons/fa6";
import { Link } from "react-router-dom";

export default function TableShow(props) {
  const currentUser = props.currentUser || false; 


  // console.log( "product " ,props.data[1].image);


  const headerShow = props.header.map((item, id) => (
    <th key={id}>{item.name} </th>
  ));
  const dataShow = props.data.map((row, id) => (
    <tr key={id}>
      <td> {id + 1}</td>
      {props.header.map((cell, id) => (
        <td key={id}>
          {
          row.key === "image" ? (
            <img src={`${row[cell.key]}`} alt="" />
          ) : row[cell.key] === "2002" ? (
            "Admin"
          ) : row[cell.key] === "2010" ? (
            "Products Managment"
          ) : row[cell.key] === "2020" ? (
            "Employee"
          ) : row[cell.key] === "2023" ? (
            "User"
          ) : (
            row[cell.key]
          )}
          {currentUser && row[cell.key] === currentUser && " (You)"}
        </td>
      ))}
      <td>
        <div className="d-flex justify-content-center align-items-center gap-3">
          <Link to={`${row._id}`}>
            <FaPenToSquare size={20} cursor={"pointer"} color="#655ae5" />
          </Link>
          {currentUser !== row.name && (
            <AiFillDelete
              size={20}
              cursor={"pointer"}
              color="#fa6680"
              onClick={() => props.delete(row._id)}
            />
          )}
        </div>
      </td>
    </tr>
  ));
  return (
  <>  <Table bordered hover responsive>
      <thead>
        <tr>
          <th> Id </th>
          {headerShow}
          <th> Action </th>
        </tr>
      </thead>
      <tbody>
        {props.data.length === 0 ? (
          <tr>
            <td colSpan={12} className="text-center">
              Loading...
            </td>
          </tr>
        ) : props.data.length <= 1 && props.noUsers ? (
          <tr>
            <td colSpan={12} className="text-center">
              No users found
            </td>
          </tr>
        ) : (
          dataShow
        )}
      </tbody>
    </Table>
    {/* <img src={props.data[7].image} alt=""  /> */}
    </>
  );
}
