import TitleSection from "../../Components/TitleSection";
import MyNavbar from "../NavbarAndFooter/Navbar";
import Goods from "../../Components/Goods";
import Footer from "../NavbarAndFooter/Footer";

// =====================
import image1 from "../../../Css/jewelry/1.jpg";
import image1closed from "../../../Css/jewelry/1closer.jpg";
import image2 from "../../../Css/jewelry/2.jpg";
import image2closed from "../../../Css/jewelry/2closer.jpg";
import image3 from "../../../Css/jewelry/3.jpg";
import image3closed from "../../../Css/jewelry/3closer.jpg";
// ====================

export default function HomePage() {
  const data = [
    {
      image1: image1,
      image2: image1closed,
      price: "12,000,000",
      karat: "24",
    },
    {
      image1: image2,
      image2: image2closed,
      price: "12,504,000",
      karat: "24",
    },
    {
      image1: image3,
      image2: image3closed,
      price: "1,009,000",
      karat: "24",
    },
  ];
  return (
    <>
      <MyNavbar />
      <div className="bg-home d-flex flex-column justify-content-center align-items-lg-start  align-items-center  w-100 p-lg-5  ">
        <span className="text-start"> NEW COLLECTION </span>
        <h1 className="text-title-responsive   text-start">
          THE NEW RING <br /> SENSATION
        </h1>
      </div>
      <TitleSection small="POPULAR PRODUCTS" big="TRENDING NOW" />
      <div
        style={{ gap: "1rem" }}
        className="d-flex flex-wrap justify-content-center my-5"
      >
        {data.map((item, key) => (
          <Goods
            key={key}
            image1={item.image1}
            image2={item.image2}
            karat={item.karat}
            price={item.price}
          />
        ))}
      </div>
      <TitleSection
        small="BEST IN BUSINESS"
        big="WHY CHOOSE US"
        middle="CRAS MALESUADA DOLOR SIT AMET EST EGESTAS ULLAMCORPER. NULLAM IN TORTOR MI. MAECENAS VULPUTATE LIBERO"
      />
      <Footer />
    </>
  );
}
