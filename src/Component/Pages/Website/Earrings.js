import Navbar from "../NavbarAndFooter/Navbar";
import Footer from "../NavbarAndFooter/Footer";
import Goods from "../../Components/Goods";

// IMPORT IMAGES
import imageFar1 from "../../../Css/earrings/1far.jpg";
import imageClosed1 from "../../../Css/earrings/1closed.jpg";
import imageFar2 from "../../../Css/earrings/2far.jpg";
import imageClosed2 from "../../../Css/earrings/2closed.jpg";
import imageFar3 from "../../../Css/earrings/3far.jpg";
import imageClosed3 from "../../../Css/earrings/3closed.jpg";
import imageFar4 from "../../../Css/earrings/4far.jpg";
import imageClosed4 from "../../../Css/earrings/4closed.jpg";
import imageFar5 from "../../../Css/earrings/5far.jpg";
import imageClosed5 from "../../../Css/earrings/5closed.jpg";
import imageFar6 from "../../../Css/earrings/6far.jpg";
import imageClosed6 from "../../../Css/earrings/6closed.jpg";
// ===============





export default function Earrings() {
  // FAKE DATA
  const data = [
    {
      imageFar: imageFar1,
      imageClosed: imageClosed1,
      karat: "18",
      price: "1,200,000",
    },
    {
      imageFar: imageFar2,
      imageClosed: imageClosed2,
      karat: "24",
      price: "1,280,000",
    },
    {
      imageFar: imageFar3,
      imageClosed: imageClosed3,
      karat: "24",
      price: "2,200,000",
    },
    {
      imageFar: imageFar4,
      imageClosed: imageClosed4,
      karat: "20",
      price: "1,200,000",
    },
    {
      imageFar: imageFar5,
      imageClosed: imageClosed5,
      karat: "18",
      price: "4,200,000",
    },
    {
      imageFar: imageFar6,
      imageClosed: imageClosed6,
      karat: "18",
      price: "4,200,000",
    },
  ];
  return (
    <>
      <Navbar />
      <h1
        className=" font-6xl height-16 pt-5 text-start m-5 d-flex align-items-end  "
      >
        EARRINGS
      </h1>
      <section style={{ margin: "5rem 0" }}>
        <div
          style={{ gap: "1rem" }}
          className="d-flex flex-wrap justify-content-center "
        >
          {data.map((item, key) => (
            <Goods
              key={key}
              image1={item.imageFar}
              image2={item.imageClosed}
              karat={item.karat}
              price={item.price}
            />
          ))}
        </div>
      </section>
      <Footer />
    </>
  );
}
