import { useState } from "react";
import { baseURL, REGISTER } from "../../../Api/Api";
import axios from "axios";
import "./Auth.css";
import LoadingSubmit from "../../Loading/Loading";
import Cookie from "cookie-universal";
import { Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function Register() {
  // states
  const [form, setForm] = useState({
    name: "",
    email: "",
    password: "",
  });
  const navigate = useNavigate();

  // Loading
  const [loading, setLoading] = useState(false);

  // Cookie
  const cookie = Cookie();

  // ERR
  const [err, setErr] = useState("");

  // handle Form Change
  function handleChange(e) {
    setForm({ ...form, [e.target.name]: e.target.value });
  }
  // handle Submit
  async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    try {
      const res = await axios.post(`${baseURL}/${REGISTER}`, form); // DATA IS FORM
      setLoading(false);
      console.log("res" , res );
      const token = res.data.token;
      console.log("token: ", token);
      const id = res.data.newUser._id;
      const name = res.data.newUser.name;
      const role = res.data.user.role;
      console.log("id: " , id );
      cookie.set("id", id);
      cookie.set("name", name);
      cookie.set("e-commerce", token);
      cookie.set("role", role);

      navigate("/dashboard/users", { replace: true });
    } catch (err) {
      setLoading(false);
      // i will remove comment when get real backend
      if (err.response.status && err.response.status === 300) {
        setErr("Email is already been taken");
      } else {
        setErr("Internal Server ERR");
      }
    }
  }
  return (
    <>
      {loading && <LoadingSubmit />}
      <div className="container">
        <div className="row" style={{ height: "100vh" }}>
          <Form className="form" onSubmit={handleSubmit}>
            <div className="custome-form">
              <h1>Register Now</h1>

              <Form.Group
                className="form-custom"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Control
                  value={form.name}
                  onChange={handleChange}
                  type="text"
                  name="name"
                  placeholder="Enter Your Name.."
                  minLength="3"
                  required
                />
                <Form.Label>Name:</Form.Label>
              </Form.Group>

              <Form.Group
                className="form-custom"
                controlId="exampleForm.ControlInput2"
              >
                <Form.Control
                  value={form.email}
                  onChange={handleChange}
                  type="email"
                  name="email"
                  placeholder="Enter Your Email.."
                  required
                />
                <Form.Label>Email:</Form.Label>
              </Form.Group>
              <Form.Group
                className="form-custom"
                controlId="exampleForm.ControlInput3"
              >
                <Form.Control
                  value={form.password}
                  onChange={handleChange}
                  type="password"
                  name="password"
                  placeholder="Enter Your Password.."
                  minLength="8"
                  required
                />
                <Form.Label>Password:</Form.Label>
              </Form.Group>
              <button className="button btnPrimary" type="submit">
                Signup
              </button>
              {err && <span className="error">{err} </span>}
            </div>
          </Form>
        </div>
      </div>
    </>
  );
}
